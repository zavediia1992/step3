

function createCard(map) {
    fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
        },
        body: JSON.stringify(Object.fromEntries(map))
    })
        .then(response => {
            if (response.ok) {

            }
        })
        .then(response => console.log(response))

}
 function getCardDataInput(map) {
     map = new Map();
     let inputs = document.querySelectorAll("[data-input]");

     inputs.forEach(item => {
        let className = item.className;
        let inputValue = item.value;
         if(inputValue !== '') {
             map.set(className, inputValue)
         }
     })
     console.log(map)
     createCard(map);
     btnClick();
 }


//let option = this.options[this.selectedIndex].getAttribute("data-option-name");

function createCardBtn() {
    let createBtn = document.querySelector('.create');
    createBtn.addEventListener('click', function (){
        let visitModal = document.querySelector('.visit-modal');
        visitModal.style.display = 'none';
        getCardDataInput();
    })
}

createCardBtn();