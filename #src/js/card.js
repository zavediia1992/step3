let visitList = document.querySelector('.visit-list');
let cardForm;
let doctor;
let patient;
//cardMoreData.classList.add('card-more__wrapper');


function renderCard(card, cardKey) {

       let p = document.createElement('p');
       let textInput = document.createElement('input');
       textInput.classList.add("input-data");
       textInput.readOnly = true;
       textInput.setAttribute("type", "text", );


    switch (card) {
        case 'patient':
            p.innerHTML = 'Фио';
            textInput.value = cardKey;
            p.classList.add('patient');
            break;
        case 'doctors':
            switch (cardKey) {
                case 'dentist':
                    textInput.value = 'Стоматолог';
                    break;
                case 'cardiologist':
                    textInput.value = 'Кардиолог';
                    break;
                case 'therapist':
                    textInput.value = 'Терапевт';
                    break;
            }
            p.innerHTML = 'Доктор';
            p.classList.add('doctor');
            break;

            case 'description':
            p.innerHTML = 'Краткое описание';
            textInput.value = cardKey;
            break;

            case 'urgency':
                switch (cardKey) {
                    case 'prioritised':
                        textInput.value = 'Приоритетная';
                        break;
                    case 'regular':
                        textInput.value = 'Обычная';
                        break;
                    case 'urgent':
                        textInput.value = 'Срочная';
                        break;
                }
            p.innerHTML = 'Срочность';
            break;

        case 'pressure':
            p.innerHTML = 'Обычное давление';
            textInput.value = cardKey;
            break;
        case 'weight-index':
            p.innerHTML = 'Индекс массы';
            textInput.value = cardKey;
            break;
        case 'sickness':
            p.innerHTML = 'Перенесенные сердечные заболевания сердечно-сосудистой системы';
            textInput.value = cardKey;
            break;
        case 'age':
            p.innerHTML = 'Возраст';
            textInput.value = cardKey;
            break;
        case 'last-visit':
            p.innerHTML = 'Дата последнего посещения';
            textInput.value = cardKey;
            break;
        case 'goal':
            p.innerHTML = 'Цель Визита';
            textInput.value = cardKey;
            break;
    }

    if (textInput.value !=='') {
        if (p.className === 'patient'){

        }
        cardForm.appendChild(p);
        p.appendChild(textInput);
        let inpData = document.querySelector('.input-data');
        inpData.readOnly = true;
    }

}
function cardMore() {
    let cardButton = document.querySelector('.card-button');
    cardButton.addEventListener('click', showMoreData)
}

function showMoreData() {
    let cardMore = document.querySelector('.card-more');
    cardMore.style.display = 'block';
}

function getCardData(card) {
    cardForm = document.createElement('form');
    let cardMoreData = document.createElement('div');
    cardMoreData.classList.add('card-more');
    let cardMoreDataWrapper = document.createElement('div');
    cardMoreData.classList.add('card-more__wrapper');
    cardMoreData.append(cardMoreDataWrapper);
    doctor = document.createElement('input');
    patient = document.createElement('input');
    cardForm.classList.add('card')
    visitList.append(cardForm);

    let btn = document.createElement('button');
    btn.classList.add('card-button');
    btn.innerText = 'Показать больше';
    cardForm.appendChild(btn);
    console.log(card);

    Object.keys(card).forEach(key => {
        console.log("this is card Checker", key, card[key]);
        renderCard(key, card[key]);

    })
}

