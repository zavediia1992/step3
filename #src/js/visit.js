document.getElementById("visit-doctor").onchange = function () {
         let modalData = document.querySelector('.visit-modal__data');
         modalData.classList.add('active');
         let option = this.options[this.selectedIndex].getAttribute("data-option-name")
         selectDocContent(option);
        console.log(option);

};
let docContent = document.querySelectorAll('.doctor');
function selectDocContent(option) {
        docContent.forEach(item => {
            if (item.classList.contains(option)){
                item.classList.add('active')
            }
            else {
                item.classList.remove('active');
            }
        })
    }
class Visit {
    constructor(doctor, selectedDoctorValue, goal, description, urgency,selectUrgencyValue,patientName) {

        doctor = document.querySelector('.doctors');
        selectedDoctorValue = doctor.options[doctor.selectedIndex].value;
        goal = document.querySelector('.goal').value;
        description = document.querySelector('.description').value;
        urgency = document.querySelector('.urgency');
        selectUrgencyValue = urgency.options[urgency.selectedIndex].value;
        patientName = document.querySelector('.patient').value;
      //  console.log(selectedDoctorValue + ' ' + goal + " " + description + " " + selectUrgencyValue + " " + patientName)

    }
}
class VisitDentist extends Visit {
    constructor(lastVisit) {
        super();
        lastVisit = document.querySelector('.last-visit').value;
       // console.log(lastVisit);
    }
}
class VisitCardiologist extends Visit {
    constructor(pressure, weightIndex, sickness, age) {
        super();
        pressure = document.querySelector('.pressure').value;
        weightIndex = document.querySelector('.weight-index').value;
        sickness = document.querySelector('.sickness').value;
        age = document.querySelector('.age').value;
      //  console.log(pressure, weightIndex, sickness, age);
    }
}
class VisitTherapist extends Visit {
    constructor(age) {
        super();
        age = document.querySelector('.doc3-age').value;

    }
}
document.querySelector('.create').addEventListener('click', checkDoctor);
function checkDoctor() {
    let doctor = document.querySelector('.doctors');
    let selectedDoctorValue = doctor.options[doctor.selectedIndex].value;
    if (selectedDoctorValue === 'dentist') {
        let createVisit = new VisitDentist;
    }
    else if (selectedDoctorValue === 'cardiologist') {
        let createVisit = new VisitCardiologist;
    }
    else if (selectedDoctorValue === 'therapist') {
        let createVisit = new VisitTherapist;
    }
}



