let accessToken;
const clickedLink = document.querySelector('.header-nav__menu-login');
const loginForm = document.querySelector('.login-form');
function loginBtn() {
    clickedLink.addEventListener("click", ()=>
       loginForm.style.display = "flex"
    )
}
loginBtn();

function btnClick() {
    const mail = document.querySelector('.mail').value;
    const password = document.querySelector('.password').value;
    fetch("https://ajax.test-danit.com/api/v2/cards/login", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({email: mail, password: password})
    })
        .then(response => {
            if (!response.ok) {
                alert('wrong credentials');
              //  showLoginModal();
                hideLoginModal();
                showLoginModalBtn();
               // showLoginModalBtn();
            }
            else {
                login();
                hideLoginModal();
                hideLoginBtn();
                displayVisit();
                openVisitModal();
                return response.text();
            }
        })
        .then(token => {
            accessToken = token;
            checkToken(accessToken);
        })
}

function checkToken(token) {
    if (token !== undefined || token !== '') {
        getIt();
        }
}

function login() {
    const login = document.querySelector('.login-btn');
    login.addEventListener("click", btnClick);
}
login();

function hideLoginModal() {
    loginForm.style.display = "none"
}

function hideLoginBtn() {
    const btnLogin = document.querySelector('.header-nav__menu-login');
    btnLogin.style.display = "none"
}
function showLoginModalBtn() {
    const btnLogin = document.querySelector('.header-nav__menu-login');
    btnLogin.style.display = "block"
}



function displayVisit() {
    const visitBtn = document.querySelector('.header-nav__menu-visit');
    visitBtn.style.display = "block";
}

function openVisitModal() {
    const visitBtn = document.querySelector('.header-nav__menu-visit');
    visitBtn.addEventListener('click', function () {
        const visitModal = document.querySelector('.visit-modal');
        visitModal.style.display = "block";
    })
}








